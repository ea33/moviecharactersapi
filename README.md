# Movie characters api

# Usage
- clone repository
- Modify appsettings.json to have your server name
- run ``Update-Database`` in NuGet Package Manage Controller

## Features
- Full CRUD for characters, movies and franchises
- Update characters in movie
- Update movies in franchise
- Get all movies in a franchise
- Get all characters in a movie

## Contributors
- Thien Nguyen
- Valtteri Elo
