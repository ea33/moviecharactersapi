﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Dtos;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Repositories;

namespace MovieCharactersAPI.Controllers
{
    [Route("Characters")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private CharacterRepo characterRepo;

        public CharacterController([FromServices] CharacterRepo repo)
        {
            this.characterRepo = repo;
        }

        [HttpGet]
        public async Task<ActionResult<List<CharacterDto>>> GetAll()
        {
            var characters = await characterRepo.GetAllCharacters();
            if (characters == null) return NotFound("No Characters in storage");
            return Ok(characters);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetById(int id)
        {
            var character = await characterRepo.GetCharacterById(id);
            if (character == null) return NotFound("Character not found");
            return Ok(character);
        }

        [HttpPost]
        public async Task<ActionResult<List<CharacterDto>>> AddCharacter([FromBody] CharacterDto characterDto)
        {
            var characters = await characterRepo.AddCharacter(characterDto);
            if (characters == null) return NotFound("No characters in storage");
            return Ok(characters);
        }

        [HttpPut]
        public async Task<ActionResult<CharacterDto>> UpdateCharacter([FromBody] CharacterDto characterDto)
        {
            var character = await characterRepo.EditCharacter(characterDto);
            if (character == null) return NotFound("Character not found");
            return Ok(character);
        }

        [HttpDelete]
        public async Task<ActionResult<List<CharacterDto>>> DeleteCharacter(int id)
        {
            var characters = await characterRepo.RemoveCharacter(id);
            if (characters == null) return NotFound("Character not found");
            return Ok(characters);
        }
    }
}
