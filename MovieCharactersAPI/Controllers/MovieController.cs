﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Dtos;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Repositories;

namespace MovieCharactersAPI.Controllers
{
    [Route("Movies")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private MovieRepo movieRepo;

        public MovieController([FromServices] MovieRepo repo)
        {
            this.movieRepo = repo;
        }

        [HttpGet]
        public async Task<ActionResult<List<MovieDto>>> GetAll()
        {
            var movies = await movieRepo.GetAllMovies();
            if (movies == null) return NotFound("No movies in storage");
            return Ok(movies);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetById(int id)
        {
            var movie = await movieRepo.GetMovieById(id);
            if (movie == null) return NotFound("Movie not found");
            return Ok(movie);
        }

        [HttpPost]
        public async Task<ActionResult<List<MovieDto>>> AddMovie([FromBody] MovieDto movieDto)
        {
            var movies = await movieRepo.AddMovie(movieDto);
            if (movies == null) return NotFound("No movies in storage");
            return Ok(movies);
        }

        [HttpPut]
        public async Task<ActionResult<MovieDto>> UpdateMovie([FromBody] MovieDto movieDto)
        {
            var movie = await movieRepo.EditMovie(movieDto);
            if (movie == null) return NotFound("Movie not found");
            return Ok(movie);
        }

        [HttpDelete]
        public async Task<ActionResult<List<MovieDto>>> DeleteMovie(int id)
        {
            var movies = await movieRepo.RemoveMovie(id);
            if (movies == null) return NotFound("Movie not found");
            return Ok(movies);
        }

        [HttpPost("Character")]
        public async Task<ActionResult<Movie>> AddCharacterToMovie([FromBody] CharacterMovieDto req)
        {
            var characterMovie = await movieRepo.AddCharacter(req);
            if (characterMovie == null) return NotFound("character or movie not found");
            return Ok(characterMovie);
        }

        [HttpDelete("Character")]
        public async Task<ActionResult<Movie>> RemoveCharacterFromMovie([FromBody] CharacterMovieDto req)
        {
            var characterMovie = await movieRepo.RemoveCharacter(req);
            if (characterMovie == null) return NotFound("character or movie not found");
            return Ok(characterMovie);
        }

        [HttpGet("Characters")]
        public async Task<ActionResult<List<CharacterDto>>> GetAllCharacters(int movieId)
        {
            var res = await movieRepo.GetAllCharacters(movieId);
            return Ok(res);
        }
    }
}
