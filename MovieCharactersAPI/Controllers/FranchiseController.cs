﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Dtos;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Repositories;

namespace MovieCharactersAPI.Controllers
{
	[Route("Franchises")]
	[ApiController]
	public class FranchiseController : ControllerBase
	{
		private FranchiseRepo franchiseRepo;

		public FranchiseController([FromServices] FranchiseRepo repo)
		{
			this.franchiseRepo = repo;
		}

		[HttpGet]
		public async Task<ActionResult<List<FranchiseDto>>> GetAll()
		{
			var franchises = await franchiseRepo.GetAllFranchises();
			if (franchises == null) return NotFound("No franchise in storage");
			return Ok(franchises);
		}

		[HttpGet("{id}")]
		public async Task<ActionResult<FranchiseDto>> GetById(int id)
		{
			var franchise = await franchiseRepo.GetFranchiseById(id);
			if (franchise == null) return NotFound("franchise not found");
			return Ok(franchise);
		}

		[HttpPost]
		public async Task<ActionResult<List<FranchiseDto>>> AddFranchise([FromBody] FranchiseDto franchiseDto)
		{
			var franchises = await franchiseRepo.AddFranchise(franchiseDto);
			if (franchises == null) return NotFound("No franchise in storage");
			return Ok(franchises);
		}

		[HttpPut]
		public async Task<ActionResult<FranchiseDto>> UpdateFranchise([FromBody] FranchiseDto franchiseDto)
		{
			var franchise = await franchiseRepo.EditFranchise(franchiseDto);
			if (franchise == null) return NotFound("franchise not found");
			return Ok(franchise);
		}

		[HttpDelete]
		public async Task<ActionResult<List<FranchiseDto>>> DeleteFranchise(int id)
		{
			var franchises = await franchiseRepo.RemoveFranchise(id);
			if (franchises == null) return NotFound("franchise not found");
			return Ok(franchises);
		}

		[HttpPost("AddMovie")]
		public async Task<ActionResult<Franchise>> AddMovieToFranchise([FromBody] MovieFranchiseDto req)
		{
			var result = await franchiseRepo.AddMovie(req);
			if (result == null) return NotFound("Franchise or movie not found");
			return result;
		}

		[HttpDelete("RemoveMovie")]
        public async Task<ActionResult<Franchise>> RemoveMovieFromFranchise([FromBody] MovieFranchiseDto req)
        {
            var result = await franchiseRepo.RemoveMovie(req);
            if (result == null) return NotFound("Franchise or movie not found");
            return result;
        }

		[HttpGet("AllMovies")]
        public async Task<ActionResult<List<MovieDto>>> GetAllMovies(int franchiseId)
        {
            var result = await franchiseRepo.GetAllMovies(franchiseId);
            return Ok(result);
        }

        [HttpGet("AllCharacters")]
        public async Task<ActionResult<List<MovieDto>>> GetAllCharacters(int franchiseId)
        {
            var result = await franchiseRepo.GetAllCharacters(franchiseId);
            return Ok(result);
        }
    }
}
