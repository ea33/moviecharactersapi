﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        { }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Franchise>().HasData(
                new Franchise
                {
                    Id = 1,
                    Name = "James Bond Franchise",
                    Description = "James Bond Description"
                },

                new Franchise
                {
                    Id = 2,
                    Name = "Batman Franchise",
                    Description = "Batman Description"
                },

                new Franchise
                {
                    Id = 3,
                    Name = "Spiderman Franchise",
                    Description = "Spiderman Description"
                }
            );

            builder.Entity<Movie>().HasData(
                new Movie
                {
                    Id = 1,
                    Title = "James Bond - No Time to Die",
                    Genre = "Action",
                    ReleaseYear = 2021,
                    Director = "Cary Fukunaga",
                    Picture = "img.net",
                    Trailer = "youtube.com",
                },
                new Movie
                {
                    Id = 2,
                    Title = "The Batman",
                    Genre = "Action, Superhero",
                    ReleaseYear = 2022,
                    Director = "Matt Reeves",
                    Picture = "img.net",
                    Trailer = "youtube.com",
                },

                new Movie
                {
                    Id = 3,
                    Title = "Spiderman - No way home",
                    Genre = "Action, Superhero",
                    ReleaseYear = 2021,
                    Director = "John Watts",
                    Picture = "img.net",
                    Trailer = "youtube.com",
                }
            );

            builder.Entity<Character>().HasData(
                new Character
                {
                    Id = 1,
                    FullName = "Tom Hollan",
                    Alias = "Spiderman",
                    Gender = "Male",
                    Picture = "img.net",

                },
                new Character
                {
                    Id = 2,
                    FullName = "Burce Wayne",
                    Alias = "Batman",
                    Gender = "Male",
                    Picture = "img.net",
                },

                new Character
                {
                    Id = 3,
                    FullName = "Commander James Bond",
                    Alias = "James Bond",
                    Gender = "Male",
                    Picture = "img.net",
                },

                new Character
                {
                    Id = 4,
                    FullName = "Zendaya Coleman",
                    Alias = "MJ",
                    Gender = "Female",
                    Picture = "img.net",
                }
            );
        }
    }
}
