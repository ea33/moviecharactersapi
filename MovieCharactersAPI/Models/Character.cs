﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Character
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Alias { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string Picture { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
