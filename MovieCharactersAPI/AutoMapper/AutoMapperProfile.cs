﻿using AutoMapper;
using MovieCharactersAPI.Dtos;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Movie, MovieDto>();
            CreateMap<MovieDto, Movie>();

            CreateMap<Character, CharacterDto>();
            CreateMap<CharacterDto, Character>();

            CreateMap<Franchise, FranchiseDto>();
            CreateMap<FranchiseDto, Franchise>();
        }
    }
}
