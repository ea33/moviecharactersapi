﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Dtos;
using MovieCharactersAPI.Models;
using System.Diagnostics;

namespace MovieCharactersAPI.Repositories
{
    public class MovieRepo
    {
        private readonly AppDbContext dbContext;
        private readonly IMapper mapper;
        public MovieRepo(AppDbContext db, IMapper mapper)
        {
            dbContext = db;
            this.mapper = mapper;
        }

        public async Task<List<MovieDto>>GetAllMovies()
        {
            List<Movie> movies = await dbContext.Movies.ToListAsync();
            if(movies.Count == 0) return null;

            return movies.Select(movie => mapper.Map<MovieDto>(movie)).ToList();
        }

        public async Task<MovieDto> GetMovieById(int id)
        {
            Movie movie = await dbContext.Movies.FindAsync(id);
            if(movie == null) return null;

            return mapper.Map<MovieDto>(movie);
        }

        public async Task<List<MovieDto>> AddMovie(MovieDto movieDto)
        {
            Movie movie = mapper.Map<Movie>(movieDto);
            dbContext.Movies.Add(movie);
            await dbContext.SaveChangesAsync();
            return await GetAllMovies();
        }

        public async Task<List<MovieDto>> RemoveMovie(int id)
        {
            Movie movie = await dbContext.Movies.FindAsync(id);
            if (movie == null) return null;
            dbContext.Movies.Remove(movie);
            await dbContext.SaveChangesAsync();
            return await GetAllMovies();
        }

        public async Task<Movie> EditMovie(MovieDto movieDto)
        {
            Movie movie = await dbContext.Movies.FindAsync(movieDto.Id);
            if (movie == null) return null;
            dbContext.Entry(movie).State = EntityState.Detached;

            movie = mapper.Map<Movie>(movieDto);
            dbContext.Movies.Update(movie);
            await dbContext.SaveChangesAsync();
            return movie;
        }

        public async Task<Movie> AddCharacter(CharacterMovieDto req)
        {
            var movie = await dbContext.Movies.FindAsync(req.MovieId);
            var character = await dbContext.Characters.FindAsync(req.CharacterId);

            if (movie == null || character == null) return null;

            movie.Characters.Add(character);
            await dbContext.SaveChangesAsync();

            Movie res = await dbContext.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == req.MovieId)
                .FirstOrDefaultAsync();

            return res;
        }

        public async Task<Movie> RemoveCharacter(CharacterMovieDto req)
        {
            Movie movie = await dbContext.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == req.MovieId)
                .FirstOrDefaultAsync();

            Character character = movie.Characters
                .Where(c => c.Id == req.CharacterId)
                .FirstOrDefault();

            if (movie == null || character == null) return null;

            movie.Characters.Remove(character);
            await dbContext.SaveChangesAsync();
            return movie;
        }

        public async Task<List<CharacterDto>> GetAllCharacters(int movieId)
        {
            List<CharacterDto> characters = new List<CharacterDto>();

            Movie movie = await dbContext.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == movieId)
                .FirstOrDefaultAsync();

            foreach (Character c in movie.Characters)
            {
                CharacterDto CDto = mapper.Map<CharacterDto>(c);
                characters.Add(CDto);
            }
            return characters;
        }

    }
}
