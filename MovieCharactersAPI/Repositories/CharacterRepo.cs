﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Dtos;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Repositories
{
    public class CharacterRepo
    {
        private readonly AppDbContext dbContext;
        private readonly IMapper mapper;
        public CharacterRepo(AppDbContext db, IMapper mapper)
        {
            dbContext = db;
            this.mapper = mapper;
        }

        public async Task<List<CharacterDto>> GetAllCharacters()
        {
            List<Character> characters = await dbContext.Characters.ToListAsync();
            if (characters.Count == 0) return null;

            return characters.Select(character => mapper.Map<CharacterDto>(character)).ToList();
        }

        public async Task<CharacterDto> GetCharacterById(int id)
        {
            Character character = await dbContext.Characters.FindAsync(id);
            if (character == null) return null;

            return mapper.Map<CharacterDto>(character);
        }

        public async Task<List<CharacterDto>> AddCharacter(CharacterDto characterDto)
        {
            Character character = mapper.Map<Character>(characterDto);
            dbContext.Characters.Add(character);
            await dbContext.SaveChangesAsync();
            return await GetAllCharacters();
        }

        public async Task<List<CharacterDto>> RemoveCharacter(int id)
        {
            Character character = await dbContext.Characters.FindAsync(id);
            if (character == null) return null;
            dbContext.Characters.Remove(character);
            await dbContext.SaveChangesAsync();
            return await GetAllCharacters();
        }

        public async Task<CharacterDto> EditCharacter(CharacterDto characterDto)
        {
            Character character = await dbContext.Characters.FindAsync(characterDto.Id);
            if (character == null) return null;
            dbContext.Entry(character).State = EntityState.Detached;

            character = mapper.Map<Character>(characterDto);
            dbContext.Characters.Update(character);
            await dbContext.SaveChangesAsync();
            return await GetCharacterById(character.Id);
        }
    }
}
