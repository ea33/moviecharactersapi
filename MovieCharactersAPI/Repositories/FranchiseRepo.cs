﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Dtos;
using MovieCharactersAPI.Models;
using System.Diagnostics;

namespace MovieCharactersAPI.Repositories
{
	public class FranchiseRepo
	{
		private readonly AppDbContext dbContext;
		private readonly IMapper mapper;
        private MovieRepo movieRepo;
		public FranchiseRepo(AppDbContext db, IMapper mapper, [FromServices] MovieRepo repo)
		{
			dbContext = db;
			this.mapper = mapper;
            movieRepo = repo;
		}

		public async Task<List<FranchiseDto>> GetAllFranchises()
		{
			List<Franchise> franchises = await dbContext.Franchises.ToListAsync();
			if (franchises.Count == 0) return null;

			return franchises.Select(franchise => mapper.Map<FranchiseDto>(franchise)).ToList();
        }

		public async Task<FranchiseDto> GetFranchiseById(int id)
		{
			Franchise franchise = await dbContext.Franchises.FindAsync(id);
			if (franchise == null) return null;

			return mapper.Map<FranchiseDto>(franchise);
		}

		public async Task<List<FranchiseDto>> AddFranchise(FranchiseDto franchiseDto)
		{
            Franchise franchise = mapper.Map<Franchise>(franchiseDto);
            dbContext.Franchises.Add(franchise);
			await dbContext.SaveChangesAsync();
			return await GetAllFranchises();
		}

		public async Task<List<FranchiseDto>> RemoveFranchise(int id)
		{
			Franchise franchise = await dbContext.Franchises.FindAsync(id);
			if (franchise == null) return null;
			dbContext.Franchises.Remove(franchise);
			await dbContext.SaveChangesAsync();
			return await GetAllFranchises();
		}

		public async Task<Franchise> EditFranchise(FranchiseDto franchiseDto)
		{
			Franchise franchise = await dbContext.Franchises.FindAsync(franchiseDto.Id);
			if (franchise == null) return null;
            dbContext.Entry(franchise).State = EntityState.Detached;
 
            franchise = mapper.Map<Franchise>(franchiseDto);
            dbContext.Franchises.Update(franchise);
            await dbContext.SaveChangesAsync();
			return franchise;
		}

		public async Task<Franchise> AddMovie(MovieFranchiseDto req)
		{
            var movie = await dbContext.Movies.FindAsync(req.MovieId);
			var franchise = await dbContext.Franchises.FindAsync(req.FranchiseId);
			if (movie == null || franchise == null) return null;

			if (!franchise.Movies.Contains(movie)){
                franchise.Movies.Add(movie);
                await dbContext.SaveChangesAsync();
            }
            Franchise res = await dbContext.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == req.FranchiseId)
                .FirstOrDefaultAsync();

			return res;
        }

        public async Task<Franchise> RemoveMovie(MovieFranchiseDto req)
        {
            Franchise franchise = await dbContext.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == req.FranchiseId)
                .FirstOrDefaultAsync();

            Movie movie = franchise.Movies
                .Where(m => m.Id == req.MovieId)
                .FirstOrDefault();

            if (movie == null || franchise == null) return null;

            franchise.Movies.Remove(movie);
            await dbContext.SaveChangesAsync();
            return franchise;
        }

        public async Task<List<MovieDto>> GetAllMovies(int franchiseId)
        {
            List<MovieDto> movies = new List<MovieDto>();

            Franchise franchise= await dbContext.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == franchiseId)
                .FirstOrDefaultAsync();

            foreach (Movie m in franchise.Movies)
            {
                MovieDto MDto = mapper.Map<MovieDto>(m);
                movies.Add(MDto);
            }
            return movies;
        }

        public async Task<List<CharacterDto>> GetAllCharacters(int franchiseId)
        {
            List<Movie> movies = new List<Movie>();
            List<CharacterDto> characterDtos = new List<CharacterDto>();

            Franchise franchise = await dbContext.Franchises
            .Include(f => f.Movies)
            .Where(f => f.Id == franchiseId)
            .FirstOrDefaultAsync();

            foreach (Movie m in franchise.Movies)
            {
                List<CharacterDto> data = await movieRepo.GetAllCharacters(m.Id);
                foreach (CharacterDto c in data)
                {
                    if (!characterDtos.Contains(c))
                    {
                        characterDtos.Add(c);
                    }
                }
            }
            return characterDtos;
        }
    }
}
