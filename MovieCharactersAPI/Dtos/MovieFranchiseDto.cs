﻿using Microsoft.Build.Framework;

namespace MovieCharactersAPI.Dtos
{
    public class MovieFranchiseDto
    {
        [Required]
        public int FranchiseId { get; set; }
        [Required]
        public int MovieId { get; set; }
    }
}
