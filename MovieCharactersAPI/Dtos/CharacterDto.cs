﻿using MovieCharactersAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Dtos
{
    public class CharacterDto
    {
        public int Id { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Alias { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string Picture { get; set; }
        //public ICollection<Movie> Movies { get; set; }
    }
}
