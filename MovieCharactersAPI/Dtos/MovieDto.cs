﻿using MovieCharactersAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Dtos
{
    public class MovieDto
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [Required]
        public string Director { get; set; }
        [Required]
        public string Picture { get; set; }
        [Required]
        public string Trailer { get; set; }
        //public ICollection<Character> Characters { get; set; }
    }
}
