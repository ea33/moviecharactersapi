﻿using Microsoft.Build.Framework;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Dtos
{
    public class CharacterMovieDto
    {
        [Required]
        public int CharacterId { get; set; }
        public Character character;
        [Required]
        public int MovieId { get; set; }
        public Movie movie;
    }
}
