﻿using MovieCharactersAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Dtos
{
	public class FranchiseDto
	{
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
		[Required]
		public string Description { get; set; }
		//public virtual ICollection<Movie>? Movies { get; set; }
	}
}
